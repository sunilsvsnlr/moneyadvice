﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodesTranche")]
    public class TrancheFields : BaseFields
    {
        public string InternalDescription { get; set; }
        public decimal Percentage { get; set; }

        public virtual SpecialCodes SpecialCode { get; set; }
        public virtual IList<MatchesFields> LstMatches { get; set; }
    }
}