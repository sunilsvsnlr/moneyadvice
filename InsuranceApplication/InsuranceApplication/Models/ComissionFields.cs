﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodeComission")]
    public class ComissionFields : BaseFields
    {
        public int ComissionId { get; set; }
        public string ComissionDescription { get; set; }
    }
}