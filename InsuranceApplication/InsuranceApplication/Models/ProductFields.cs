﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodeProducts")]
    public class ProductFields : BaseFields
    {
        public int ProductId { get; set; }
        public string ProductDescription { get; set; }
    }
}