﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodesMatches")]
    public class MatchesFields : BaseFields
    {
        public int MatchId { get; set; }
        public string MatchDescription { get; set; }

        public virtual SpecialCodes SplCode { get; set; }
        public virtual TrancheFields TrancheCode { get; set; }
    }
}