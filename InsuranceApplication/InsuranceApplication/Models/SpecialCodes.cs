﻿using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodes")]
    public class SpecialCodes : BaseFields
    {
        public int TypeId { get; set; }
        public string TypeDescription { get; set; }
        public int StatusId { get; set; }
        public string StatusDescription { get; set; }
        public int Quantity { get; set; }

        public string OnlineOrVoccherDescription { get; set; }

        [MaxLength(2)]
        public string Indexation { get; set; }
        
        [MaxLength(2)]
        public string Conversion { get; set; }
        
        [MaxLength(2)]
        public string SimCan { get; set; }
        public string Deferred { get; set; }
        public string PayFreq { get; set; }
        public decimal APMin { get; set; }
        public decimal APMax { get; set; }
        public int AgeMin { get; set; }
        public int AgeMax { get; set; }
        public decimal SumAssuredMin { get; set; }
        public decimal SumAssuredMax { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string AppliesTo { get; set; }
        public string Exclusions { get; set; }

        public virtual IList<TrancheFields> LstTranches { get; set; }
        public virtual IList<MatchesFields> LstMatches { get; set; }
        public virtual IList<ComissionFields> LstComissions { get; set; }
        public virtual IList<ProductFields> LstProducts { get; set; }
        public virtual IList<BenefitFields> LstBenefits { get; set; }
        public virtual IList<LifeBasisFields> LstLifeBasis { get; set; }

    }
}