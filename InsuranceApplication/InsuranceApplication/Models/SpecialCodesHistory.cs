﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodesHistory")]
    public class SpecialCodesHistory : BaseFields
    {
        public int CodeID { get; set; }
        public int TypeId { get; set; }
        public string TypeDescription { get; set; }
        public int StatusId { get; set; }
        public string StatusDescription { get; set; }
        public int Quantity { get; set; }

        public int Tranche { get; set; }

        public string OnlineOrVocherDescription { get; set; }
        public string InternalDescription { get; set; }
        public decimal Percentage { get; set; }

        public string Match { get; set; }

        public string Product { get; set; }
        public string Benefit { get; set; }
        
        [MaxLength(2)]
        public string Indexation { get; set; }

        [MaxLength(2)]
        public string Conversion { get; set; }

        [MaxLength(2)]
        public string SimCan { get; set; }
        public string Deferred { get; set; }
        public string Comission { get; set; }
        public string LifeBasis { get; set; }
        public string PayFreq { get; set; }
        public decimal APMin { get; set; }
        public decimal APMax { get; set; }
        public int AgeMin { get; set; }
        public int AgeMax { get; set; }
        public decimal SumAssuredMin { get; set; }
        public decimal SumAssuredMax { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string AppliesTo { get; set; }
        public string Exclusions { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual SpecialCodes SplCode { get; set; }
    }
}