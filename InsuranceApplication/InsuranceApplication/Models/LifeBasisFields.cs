﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodeLifeBasis")]
    public class LifeBasisFields : BaseFields
    {
        public int LifeBasisId { get; set; }
        public string LifeBasisDescription { get; set; }
    }
}