﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Models
{
    [Table("tblSplCodeBenefits")]
    public class BenefitFields : BaseFields
    {
        public int BenefitId { get; set; }
        public string BenefitDescription { get; set; }
    }
}