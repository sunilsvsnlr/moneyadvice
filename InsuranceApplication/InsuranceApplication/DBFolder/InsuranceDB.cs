﻿using InsuranceApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InsuranceApplication.DBFolder
{
    public class InsuranceDB : DbContext
    {
        public InsuranceDB()
            : base("DefaultConnection")
        {
            Database.SetInitializer<InsuranceDB>(new CreateDatabaseIfNotExists<InsuranceDB>());
            Database.SetInitializer<InsuranceDB>(new MigrateDatabaseToLatestVersion<InsuranceDB, Migrations.Configuration>());
        }

        public DbSet<SpecialCodes> tblSplCodes { get; set; }
        public DbSet<TrancheFields> tblSplCodeTranche { get; set; }
        public DbSet<MatchesFields> tblSplCodeMatches { get; set; }
        public DbSet<ComissionFields> tblSplCodeComission { get; set; }
        public DbSet<ProductFields> tblSplCodeProducts { get; set; }
        public DbSet<BenefitFields> tblSplCodeBenefits { get; set; }
        public DbSet<LifeBasisFields> tblSplCodeLifeBasis { get; set; }
        public DbSet<SpecialCodesHistory> tblSplCodeHistory { get; set; }
    }
}