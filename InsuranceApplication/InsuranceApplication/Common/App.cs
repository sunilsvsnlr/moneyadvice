﻿using InsuranceApplication.DBFolder;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsuranceApplication.Common
{
    public class App
    {
        public static InsuranceDB insuranceDBContext = new InsuranceDB();
        public static Logger Logger = LogManager.GetLogger("MoneyAdviceAPI");
    }
}