namespace InsuranceApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tblSplCodeBenefits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BenefitId = c.Int(nullable: false),
                        BenefitDescription = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                        SpecialCodes_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblSplCodes", t => t.SpecialCodes_Id)
                .Index(t => t.SpecialCodes_Id);
            
            CreateTable(
                "dbo.tblSplCodeComission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ComissionId = c.Int(nullable: false),
                        ComissionDescription = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                        SpecialCodes_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblSplCodes", t => t.SpecialCodes_Id)
                .Index(t => t.SpecialCodes_Id);
            
            CreateTable(
                "dbo.tblSplCodesHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CodeID = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                        TypeDescription = c.String(),
                        StatusId = c.Int(nullable: false),
                        StatusDescription = c.String(),
                        Quantity = c.Int(nullable: false),
                        Tranche = c.Int(nullable: false),
                        OnlineOrVocherDescription = c.String(),
                        InternalDescription = c.String(),
                        Percentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Match = c.String(),
                        Product = c.String(),
                        Benefit = c.String(),
                        Indexation = c.String(maxLength: 2),
                        Conversion = c.String(maxLength: 2),
                        SimCan = c.String(maxLength: 2),
                        Deferred = c.String(),
                        Comission = c.String(),
                        LifeBasis = c.String(),
                        PayFreq = c.String(),
                        APMin = c.Decimal(nullable: false, precision: 18, scale: 2),
                        APMax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgeMin = c.Int(nullable: false),
                        AgeMax = c.Int(nullable: false),
                        SumAssuredMin = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SumAssuredMax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValidFrom = c.DateTime(nullable: false),
                        ValidTo = c.DateTime(nullable: false),
                        AppliesTo = c.String(),
                        Exclusions = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                        SplCode_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblSplCodes", t => t.SplCode_Id)
                .Index(t => t.SplCode_Id);
            
            CreateTable(
                "dbo.tblSplCodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        TypeDescription = c.String(),
                        StatusId = c.Int(nullable: false),
                        StatusDescription = c.String(),
                        Quantity = c.Int(nullable: false),
                        OnlineOrVoccherDescription = c.String(),
                        Indexation = c.String(maxLength: 2),
                        Conversion = c.String(maxLength: 2),
                        SimCan = c.String(maxLength: 2),
                        Deferred = c.String(),
                        PayFreq = c.String(),
                        APMin = c.Decimal(nullable: false, precision: 18, scale: 2),
                        APMax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgeMin = c.Int(nullable: false),
                        AgeMax = c.Int(nullable: false),
                        SumAssuredMin = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SumAssuredMax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValidFrom = c.DateTime(nullable: false),
                        ValidTo = c.DateTime(nullable: false),
                        AppliesTo = c.String(),
                        Exclusions = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tblSplCodeLifeBasis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LifeBasisId = c.Int(nullable: false),
                        LifeBasisDescription = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                        SpecialCodes_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblSplCodes", t => t.SpecialCodes_Id)
                .Index(t => t.SpecialCodes_Id);
            
            CreateTable(
                "dbo.tblSplCodesMatches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MatchId = c.Int(nullable: false),
                        MatchDescription = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                        SplCode_Id = c.Int(),
                        TrancheCode_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblSplCodes", t => t.SplCode_Id)
                .ForeignKey("dbo.tblSplCodesTranche", t => t.TrancheCode_Id)
                .Index(t => t.SplCode_Id)
                .Index(t => t.TrancheCode_Id);
            
            CreateTable(
                "dbo.tblSplCodesTranche",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InternalDescription = c.String(),
                        Percentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                        SpecialCode_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblSplCodes", t => t.SpecialCode_Id)
                .Index(t => t.SpecialCode_Id);
            
            CreateTable(
                "dbo.tblSplCodeProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        ProductDescription = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Deleted = c.Int(nullable: false),
                        SpecialCodes_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblSplCodes", t => t.SpecialCodes_Id)
                .Index(t => t.SpecialCodes_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tblSplCodesHistory", "SplCode_Id", "dbo.tblSplCodes");
            DropForeignKey("dbo.tblSplCodeProducts", "SpecialCodes_Id", "dbo.tblSplCodes");
            DropForeignKey("dbo.tblSplCodesTranche", "SpecialCode_Id", "dbo.tblSplCodes");
            DropForeignKey("dbo.tblSplCodesMatches", "TrancheCode_Id", "dbo.tblSplCodesTranche");
            DropForeignKey("dbo.tblSplCodesMatches", "SplCode_Id", "dbo.tblSplCodes");
            DropForeignKey("dbo.tblSplCodeLifeBasis", "SpecialCodes_Id", "dbo.tblSplCodes");
            DropForeignKey("dbo.tblSplCodeComission", "SpecialCodes_Id", "dbo.tblSplCodes");
            DropForeignKey("dbo.tblSplCodeBenefits", "SpecialCodes_Id", "dbo.tblSplCodes");
            DropIndex("dbo.tblSplCodeProducts", new[] { "SpecialCodes_Id" });
            DropIndex("dbo.tblSplCodesTranche", new[] { "SpecialCode_Id" });
            DropIndex("dbo.tblSplCodesMatches", new[] { "TrancheCode_Id" });
            DropIndex("dbo.tblSplCodesMatches", new[] { "SplCode_Id" });
            DropIndex("dbo.tblSplCodeLifeBasis", new[] { "SpecialCodes_Id" });
            DropIndex("dbo.tblSplCodesHistory", new[] { "SplCode_Id" });
            DropIndex("dbo.tblSplCodeComission", new[] { "SpecialCodes_Id" });
            DropIndex("dbo.tblSplCodeBenefits", new[] { "SpecialCodes_Id" });
            DropTable("dbo.tblSplCodeProducts");
            DropTable("dbo.tblSplCodesTranche");
            DropTable("dbo.tblSplCodesMatches");
            DropTable("dbo.tblSplCodeLifeBasis");
            DropTable("dbo.tblSplCodes");
            DropTable("dbo.tblSplCodesHistory");
            DropTable("dbo.tblSplCodeComission");
            DropTable("dbo.tblSplCodeBenefits");
        }
    }
}
